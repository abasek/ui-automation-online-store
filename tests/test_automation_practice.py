from assertpy import assert_that

from pages.cart_page import CartPage
from pages.confirmation_popup import ConfirmationPopUp
from pages.contact_page import ContactPage
from pages.main_page import MainPage
from pages.my_account_page import MyAccount
from pages.quick_view_popup import QuickViewPopUp
from pages.search_page import SearchPage
from pages.sign_in_page import SignIn
from pages.header import Header
from utils import config


def test_add_product_to_cart(random_product, driver):
    main_page = MainPage(driver)
    main_page.add_product_to_cart(random_product.id)
    confirmation_popup = ConfirmationPopUp(driver)
    confirmation_popup.close_popup()
    header = Header(driver)
    header.go_to_cart()
    cart_page = CartPage(driver)
    assert cart_page.get_first_product_name() == random_product.name


def test_add_product_with_details_selection_via_quick_view_to_cart(driver, random_product):
    main_page = MainPage(driver)
    main_page.open_product_quick_view(random_product.id)
    quick_view_popup = QuickViewPopUp(driver)
    quick_view_popup.choose_quantity(random_product.quantity)
    quick_view_popup.choose_size(random_product.size)
    quick_view_popup.add_to_cart()
    confirmation_popup = ConfirmationPopUp(driver)
    assert confirmation_popup.get_text() == "Product successfully added to your shopping cart"
    assert confirmation_popup.get_product_name() == random_product.name
    assert random_product.size in confirmation_popup.get_product_attributes()
    assert confirmation_popup.get_product_quantity() == str(random_product.quantity)
    confirmation_popup.close_popup()
    header = Header(driver)
    header.go_to_cart()
    cart_page = CartPage(driver)
    assert cart_page.get_first_product_name() == random_product.name
    cart_page.delete_product()
    assert cart_page.get_empty_notification() == "Your shopping cart is empty."


def test_send_message_to_helpdesk(driver):
    header = Header(driver)
    header.go_to_contact_page()
    contact_page = ContactPage(driver)
    contact_page.choose_random_subject()
    contact_page.enter_email()
    contact_page.enter_order_reference()
    contact_page.attach_file()
    contact_page.enter_message()
    contact_page.send_message()
    assert contact_page.get_confirmation_message() == "Your message has been successfully sent to our team."


def test_sign_in_and_sign_out_using_default_pass(driver):
    header = Header(driver)
    header.go_to_sign_in_page()
    sign_in_page = SignIn(driver)
    sign_in_page.enter_default_email()
    sign_in_page.enter_default_password()
    sign_in_page.sign_in()
    my_account_page = MyAccount(driver)
    assert my_account_page.get_my_account_heading() == 'MY ACCOUNT'
    assert header.get_user_name() == config.default_user_name
    header.sign_out()
    assert header.get_sign_in_button_text() == 'Sign in'


# assumption that only product name should be verified. The test fails which is correct
def test_search_and_verify_hint_list(driver):
    main_page = MainPage(driver)
    all_displayed_items = main_page.enter_search_text_and_get_categories_shown('Chiffon')
    all_items_with_search_text = [item for item in all_displayed_items if 'Chiffon' in item]
    assert all_displayed_items == all_items_with_search_text


def test_search_and_verify_results(driver):
    main_page = MainPage(driver)
    main_page.search_text('Chiffon')
    search_page = SearchPage(driver)
    all_displayed_names = search_page.get_all_result_names()
    all_names_with_search_text = [name for name in all_displayed_names if 'Chiffon' in name]
    assert all_displayed_names == all_names_with_search_text
