import os
import random

import pytest
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from utils import config
from utils.helpers import extract_product_ids, take_screenshot, extract_product_names


@pytest.fixture(params=["chrome"])  # "firefox"
def driver(request, screenshots_directory) -> WebDriver:
    if request.param == 'firefox':
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    else:
        driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get(config.url)
    driver.maximize_window()
    yield driver
    if request.node.rep_call.failed:
        take_screenshot(driver, screenshots_directory)
        print("executing test failed", request.node.nodeid)
    driver.quit()


@pytest.fixture(scope='session', autouse=True)
def screenshots_directory() -> str:
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'screenshots'))
    if not os.path.exists(path):
        os.makedirs(path)
    return path


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)


class Product:
    def __init__(self):
        self.id = None
        self.name = None
        self.size = None
        self.quantity = None


@pytest.fixture(scope="session")
def get_all_product_details() -> [Product]:
    main_page = requests.get(config.url)
    soup = BeautifulSoup(main_page.content, 'html.parser')
    all_product_id_links = soup.select("ul#homefeatured li a.quick-view")
    all_product_name_tags = soup.select("ul#homefeatured div.product-container a.product-name")
    product_ids = extract_product_ids(all_product_id_links)
    product_names = extract_product_names(all_product_name_tags)
    product_sizes = ["S", "M", "L"]
    product_quantities = list(range(1, 6))
    products = []
    for n in range(len(product_ids)):
        product = Product()
        product.id = product_ids[n]
        product.name = product_names[n]
        product.size = random.choice(product_sizes)
        product.quantity = random.choice(product_quantities)
        products.append(product)
    return products


@pytest.fixture(scope="function")
def random_product(get_all_product_details) -> Product:
    return random.choice(get_all_product_details)
