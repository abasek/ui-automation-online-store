import os
import random
import string
import time
from time import sleep


def extract_product_id(product_id_link_string: str) -> str:
    start = 'id_product='
    extracted_product_id = product_id_link_string[
                           product_id_link_string.find(start) + len(start):product_id_link_string.find(start) + len(
                               start) + 1]
    return extracted_product_id


def extract_product_ids(all_product_id_links) -> [str]:
    all_product_ids_list = []
    for product_id_link in all_product_id_links:
        product_id = extract_product_id(str(product_id_link))
        all_product_ids_list.append(product_id)
    return all_product_ids_list


def extract_product_names(all_product_name_tags) -> [str]:
    return [product_name.text.strip('\n\t') for product_name in all_product_name_tags if product_name.text != '']


def generate_random_email(prefix="", limit=5) -> str:
    letters = string.ascii_letters
    random_string = ''.join(random.choice(letters) for i in range(limit))
    random_email = f'{random_string}@{random_string}.com'
    return prefix + random_email


def generate_random_string(prefix="", limit=10) -> str:
    letters = string.ascii_letters
    random_string = ''.join(random.choice(letters) for i in range(limit))
    return prefix + random_string


def test_files_directory() -> str:
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../testfiles'))
    return path


def take_screenshot(driver, screenshots_directory):
    sleep(1)
    file_name = f'{time.time()}.png'
    file_path = os.path.abspath(os.path.join(screenshots_directory, file_name))
    driver.save_screenshot(file_path)


def choose_random_file() -> str:
    test_files = os.listdir(test_files_directory())
    return random.choice(test_files)
