from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class MainPage:
    add_to_cart_button = (By.CSS_SELECTOR, 'a[title="Add to cart"')
    quick_view_button = (By.CSS_SELECTOR, 'ul#homefeatured .quick-view span')
    automation_practice_website_heading = (By.CSS_SELECTOR, "p#editorial_image_legend + h1")
    all_product_id_links_selector = (By.CSS_SELECTOR, "ul#homefeatured li a.quick-view")
    all_product_names_selector = (By.CSS_SELECTOR, "div.product-container a.product-name")
    search_input = (By.CSS_SELECTOR, 'input#search_query_top')
    search_button = (By.CSS_SELECTOR, "button[name='submit_search']")
    search_results_list = (By.CSS_SELECTOR, 'div.ac_results ul li')

    product_image_selector_template = '#homefeatured a.product_img_link[href*="id_product={}"] img'
    product_quick_view_button_selector_template = 'ul#homefeatured .quick-view[href*="id_product={}"] span'
    add_to_cart_button_selector_template = '#homefeatured a[href*="id_product={}"][title="Add to cart"]'

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def add_product_to_cart(self, product_id):
        ActionChains(self.driver).move_to_element(self.__get_product_image_element(product_id)).perform()
        add_to_cart_button_selector = (
            By.CSS_SELECTOR, self.add_to_cart_button_selector_template.format(product_id))
        self.driver.find_element(*add_to_cart_button_selector).click()

    def enter_search_text_and_get_categories_shown(self, text: str) -> [str]:
        self.driver.find_element(*self.search_input).send_keys(text)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(self.search_results_list))
        return [element.text for element in (self.driver.find_elements(*self.search_results_list))]

    def search_text(self, text):
        self.driver.find_element(*self.search_input).send_keys(text)
        self.driver.find_element(*self.search_button).click()

    def open_product_quick_view(self, product_id):
        ActionChains(self.driver).move_to_element(self.__get_product_image_element(product_id)).perform()
        product_image_selector = (By.CSS_SELECTOR, self.product_quick_view_button_selector_template.format(product_id))
        self.driver.find_element(*product_image_selector).click()

    def get_automation_practice_website_heading(self) -> str:
        return self.driver.find_element(*self.automation_practice_website_heading).text

    def __get_product_image_element(self, product_id) -> WebElement:
        product_image_selector = (By.CSS_SELECTOR, self.product_image_selector_template.format(product_id))
        return self.driver.find_element(*product_image_selector)
