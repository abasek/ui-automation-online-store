from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from utils import config


class SignIn:
    email_input = (By.CSS_SELECTOR, 'input#email')
    password_input = (By.CSS_SELECTOR, 'input#passwd')
    sign_in_button = (By.CSS_SELECTOR, 'button#SubmitLogin')

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def enter_default_email(self):
        self.driver.find_element(*self.email_input).send_keys(config.default_email)

    def enter_default_password(self):
        self.driver.find_element(*self.password_input).send_keys(config.default_password)

    def sign_in(self):
        self.driver.find_element(*self.sign_in_button).click()
