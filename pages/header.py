from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class Header:
    cart_button = (By.XPATH, '//a[@title="View my shopping cart"]')
    contact_us_button = (By.CSS_SELECTOR, '#contact-link > a')
    sign_in_button = (By.CSS_SELECTOR, 'a.login')
    sign_out_button = (By.CSS_SELECTOR, '.logout')
    user_name = (By.CSS_SELECTOR, '.header_user_info a span')

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def go_to_cart(self):
        self.driver.find_element(*self.cart_button).click()

    def go_to_contact_page(self):
        self.driver.find_element(*self.contact_us_button).click()

    def go_to_sign_in_page(self):
        self.driver.find_element(*self.sign_in_button).click()

    def sign_out(self):
        self.driver.find_element(*self.sign_out_button).click()

    def get_user_name(self) -> str:
        return self.driver.find_element(*self.user_name).text

    def get_sign_in_button_text(self) -> str:
        return self.driver.find_element(*self.sign_in_button).text
