from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class MyAccount:
    my_account_heading = (By.CSS_SELECTOR, 'h1.page-heading')
    home_button = (By.CSS_SELECTOR, "a[title='Home']")

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def get_my_account_heading(self) -> str:
        return self.driver.find_element(*self.my_account_heading).text

    def go_home(self):
        self.driver.find_element(*self.home_button).click()
