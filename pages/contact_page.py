import os.path
import random

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from utils.helpers import generate_random_email, generate_random_string, test_files_directory, choose_random_file


class ContactPage:
    subject_dropdown = (By.CSS_SELECTOR, '#id_contact')
    subject_options = (By.XPATH, "//select[@id='id_contact']/option")
    email = (By.CSS_SELECTOR, 'input#email')
    order_reference = (By.CSS_SELECTOR, 'input#id_order')
    choose_file_button = (By.XPATH, "//span[@class='action']")
    choose_file_input = (By.CSS_SELECTOR, 'input#fileUpload')
    message = (By.CSS_SELECTOR, '#message')
    send = (By.CSS_SELECTOR, '#submitMessage')
    confirmation_message = (By.XPATH, "//p[@class='alert alert-success']")

    subject_option_template = "//select[@id='id_contact']//option[text()='{}']"

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def choose_random_subject(self):
        random_subject_selector = (By.XPATH, self.subject_option_template.format(self.__get_random_subject()))
        self.driver.find_element(*self.subject_dropdown).click()
        self.driver.find_element(*random_subject_selector).click()

    def enter_email(self):
        self.driver.find_element(*self.email).send_keys(generate_random_email('test'))

    def enter_order_reference(self):
        self.driver.find_element(*self.order_reference).send_keys(generate_random_string('', 5))

    def attach_file(self):
        file_path = os.path.abspath(os.path.join(test_files_directory(), choose_random_file()))
        self.driver.find_element(*self.choose_file_input).send_keys(file_path)

    def enter_message(self):
        self.driver.find_element(*self.message).send_keys(generate_random_string('', 100))

    def send_message(self):
        self.driver.find_element(*self.send).click()

    def get_confirmation_message(self) -> str:
        self.__wait_until_loaded()
        return self.driver.find_element(*self.confirmation_message).text

    def __wait_until_loaded(self):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located(self.confirmation_message))

    def __get_random_subject(self) -> str:
        self.driver.find_element(*self.subject_dropdown).click()
        all_subjects = self.driver.find_elements(*self.subject_options)
        all_subjects_list = [subject.text for subject in all_subjects if
                             subject.text != '-- Choose --']
        subject = random.choice(all_subjects_list)
        return subject
