from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class CartPage:
    first_product_description = (By.CSS_SELECTOR, ".cart_description .product-name")
    trash_icon = (By.XPATH, "//tr[1]//td//a//i[1][@class='icon-trash']")
    shopping_cart_is_empty_notification = (By.XPATH, "//p[@class='alert alert-warning']")

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def get_first_product_name(self) -> str:
        return WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located(self.first_product_description)).text

    def delete_product(self):
        WebDriverWait(self.driver, 15).until(EC.presence_of_element_located(self.trash_icon)).click()

    def get_empty_notification(self) -> str:
        return WebDriverWait(self.driver, 15).until(
            EC.visibility_of_element_located(self.shopping_cart_is_empty_notification)).text
