from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class SearchPage:
    product_name_elements = (By.CSS_SELECTOR, "ul[class='product_list grid row'] a.product-name")
    product_description_elements = (By.CSS_SELECTOR, "ul[class='product_list grid row'] p.product-desc")

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def get_all_result_names(self):
        return [element.text for element in self.driver.find_elements(*self.product_name_elements)]
