from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ConfirmationPopUp:
    layer_cart_confirmation = (By.XPATH, "//div[@id='layer_cart']//span[@class='cross']/following-sibling::h2")
    layer_cart_product_name = (By.ID, "layer_cart_product_title")
    layer_cart_product_attributes = (By.ID, "layer_cart_product_attributes")
    layer_cart_product_quantity = (By.ID, "layer_cart_product_quantity")
    cross_button = (By.CSS_SELECTOR, "span.cross")

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.__wait_until_loaded()

    def get_text(self) -> str:
        return self.driver.find_element(*self.layer_cart_confirmation).text

    def close_popup(self):
        self.driver.find_element(*self.cross_button).click()

    def get_product_attributes(self) -> str:
        return self.driver.find_element(*self.layer_cart_product_attributes).text

    def get_product_quantity(self) -> str:
        return self.driver.find_element(*self.layer_cart_product_quantity).text

    def get_product_name(self) -> str:
        return self.driver.find_element(*self.layer_cart_product_name).text

    def __wait_until_loaded(self):
        WebDriverWait(self.driver, 15).until(EC.element_to_be_clickable(self.cross_button))
