from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class QuickViewPopUp:
    quantity_field_selector = (By.ID, 'quantity_wanted')
    size_field_selector = (By.XPATH, "//select[@id='group_1']")
    size_fields_selector = (By.XPATH, "//select[@id='group_1']/option")
    size_field_M_option = (By.CSS_SELECTOR, "select#group_1 option[title='M']")
    add_to_cart_selector = (By.CSS_SELECTOR, "#add_to_cart button")
    quick_view_iframe = (By.CSS_SELECTOR, "iframe.fancybox-iframe")

    size_option_selector_template = "select#group_1 option[title='{}']"

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.__switch_to_iframe_popup()

    def get_product_sizes(self) -> list[str]:
        self.driver.find_element(*self.size_field_selector).click()
        all_product_sizes = self.driver.find_elements(*self.size_fields_selector)
        return [product_size.text for product_size in all_product_sizes]

    def choose_quantity(self, quantity: int):
        quantity_field = self.driver.find_element(*self.quantity_field_selector)
        quantity_field.click()
        quantity_field.clear()
        quantity_field.send_keys(quantity)

    def choose_size(self, size: str):
        size_selector = (By.CSS_SELECTOR, self.size_option_selector_template.format(size.upper()))
        self.driver.find_element(*self.size_field_selector).click()
        self.driver.find_element(*size_selector).click()

    def add_to_cart(self):
        self.driver.find_element(*self.add_to_cart_selector).click()
        WebDriverWait(self.driver, 20).until(EC.invisibility_of_element_located(self.quick_view_iframe))
        self.driver.switch_to.default_content()

    def __switch_to_iframe_popup(self):
        iframe_element = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(self.quick_view_iframe))
        self.driver.switch_to.frame(iframe_element)
